@FX/swimming.aiff { 
  Author = timgormly
  URL = https://freesound.org/people/timgormly/sounds/151280/
}
@FX/green-floor.wav {
  Author = swuing
  URL = https://freesound.org/people/swuing/sounds/38874/
}
@FX/grass.wav {
  Author = morganpurkis
  URL = https://freesound.org/people/morganpurkis/sounds/396013/
}
@BGM/swimming.mp3 {
  Author = JazzyBay
  URL = https://freesound.org/people/JazzyBay/sounds/435055/
}
@BGM/birds.wav {
  Author = EduFigg
  URL = https://freesound.org/people/EduFigg/sounds/266376/
}