﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move_char : MonoBehaviour
{
  private CharacterController charController;
  private Vector3 moveDirection = Vector3.zero;

  public float speed = 7.0f;
  public float jumpSpeed = 7.0f;
  public float gravity = 20.0f;
  public float rotateSpeed = 2.5f*1.5f;

  void Start()
  {
    charController = GetComponent<CharacterController>();
  }

  void Update()
  {
    if (charController.isGrounded)
    {
      // just moving forwards, or backwards
      moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
      moveDirection = transform.TransformDirection(moveDirection);
      moveDirection *= speed;
      // jump
      if (Input.GetButton("Jump"))
        moveDirection.y = jumpSpeed;
    }
    // rotate left and right. Moving left and right isn't necessary
    transform.Rotate(0, Input.GetAxis("Mouse X") * rotateSpeed, 0);

    // move and fall
    moveDirection.y -= gravity * Time.deltaTime;
    charController.Move(moveDirection * Time.deltaTime);
  }
}
