﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fx_switch_footsteps : MonoBehaviour
{
  public AudioClip oldTrack;
  public AudioClip newTrack;
  private fx_footsteps fxf;
  private fx_npc_footsteps fxnpcf;
  public float volumeCorrection;
  public float pitchCorrection;

  void Start()
  {
    fxf = FindObjectOfType<fx_footsteps>();
    fxnpcf = FindObjectOfType<fx_npc_footsteps>();
  }

  private void OnTriggerEnter(Collider other)
  {
    if (other.tag == "Player")
    {
      if (newTrack != null)
      {
        fxf.ChangeFX(newTrack);
        fxf.ChangeVolume(volumeCorrection);
        fxf.ChangePitch(pitchCorrection);
      }
    }
    else if (other.tag == "NPC")
    {
      if (newTrack != null)
      {
        fxnpcf.ChangeFX(newTrack);
        fxnpcf.ChangeVolume(volumeCorrection);
        fxnpcf.ChangePitch(pitchCorrection);
      }
    }
  }

  private void OnTriggerExit(Collider other)
  {
    if (other.tag == "Player")
    {
      if (oldTrack != null)
      {
        fxf.ChangeFX(oldTrack);
        fxf.ChangeVolume(fxf.GetVolume());
        fxf.ChangePitch(fxf.GetPitch());
      }
    }
    else if (other.tag == "NPC")
    {
      if (oldTrack != null)
      {
        fxnpcf.ChangeFX(oldTrack);
        fxnpcf.ChangeVolume(fxnpcf.GetVolume());
        fxnpcf.ChangePitch(fxnpcf.GetPitch());
      }
    }
  }
}
