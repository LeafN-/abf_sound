﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fx_footsteps : MonoBehaviour
{
  private CharacterController charController;
  public AudioSource fx = new AudioSource();
  private float vol = 0f;
  private float pit = 0f;
  public float volumeCorrection;
  public float pitchCorrection;

  void Start()
  {
    charController = GetComponent<CharacterController>();
    fx = GetComponent<AudioSource>();
    vol = volumeCorrection;
    pit = pitchCorrection;
  }

  void Update()
  {
    if (fx.clip.name == "grass" && charController.velocity.magnitude > 2f && !fx.isPlaying)
    {
      fx.volume = Random.Range(0.8f, 1f) - vol;
      fx.pitch = Random.Range(1.3f, 1.6f) + pit;
      fx.Play();
    }
    else if (charController.isGrounded && charController.velocity.magnitude > 2f && !fx.isPlaying)
    {
      fx.volume = Random.Range(0.8f, 1f) - vol;
      fx.pitch = Random.Range(1.3f, 1.6f) + pit;
      fx.Play();
    }
    if (charController.velocity.magnitude < 0.1f && fx.pitch <= 1.8f)
      fx.pitch += 0.2f;
  }

  public void ChangeFX(AudioClip music)
  {
    fx.clip = music;
  }

  public void ChangeVolume(float n)
  {
    vol = n;
  }

  public void ChangePitch(float n)
  {
    pit = n;
  }

  public float GetVolume()
  {
    return volumeCorrection;
  }

  public float GetPitch()
  {
    return pitchCorrection;
  }
}
