﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move_npc : MonoBehaviour
{
  private CharacterController charController;
  private Vector3 moveDirection = Vector3.zero;
  private float t = 0f;
  private float rotation = 0f;
  private List<float> list_mv = new List<float>();
  private List<float> list_r = new List<float>();
  private int delay = 0;

  public float speed = 7.0f;
  public float jumpSpeed = 7.0f;
  public float gravity = 20.0f;
  public float rotateSpeed = 2.5f;

  void Start()
  {
    charController = GetComponent<CharacterController>();
    list_mv.Add(-1f);
    list_mv.Add(0f);
    list_mv.Add(1f);
    list_r.Add(-1f);
    list_r.Add(1f);
  }

  void Update()
  {
    if (charController.isGrounded)
    {
      // just moving forwards, or backwards
      moveDirection = Vector3.forward;
      moveDirection = transform.TransformDirection(moveDirection);
      moveDirection *= speed;
      // jump
      if (Vector3.Distance(GameObject.Find("leafeon").transform.position,
        GameObject.Find("npc_leafeon").transform.position) <= 1f ||
        charController.velocity.magnitude < 1f)
        moveDirection.y = jumpSpeed;
    }
    // rotate left and right. Moving left and right isn't necessary
    t += Time.deltaTime;
    if (t >= 1f)
    {
      t = 0f;
      if (charController.velocity.magnitude < 1.5f || delay > 0)
      {
        if (delay < 3)
          delay += 1;
        else
          delay = 0;
        if (delay == 1)
          rotation = list_r[Mathf.RoundToInt(Random.Range(0, list_r.Count))];
      }
      else if (delay == 0)
        rotation = list_mv[Mathf.RoundToInt(Random.Range(0, list_mv.Count))];
    }
    // TODO if velocity zu klein, rotiere davon weg
    transform.Rotate(0, rotation * rotateSpeed, 0);

    // move and fall
    moveDirection.y -= gravity * Time.deltaTime;
    charController.Move(moveDirection * Time.deltaTime);
  }
}