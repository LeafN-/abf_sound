﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move_cam : MonoBehaviour
{
  [SerializeField]
  public Transform target;
  [SerializeField]
  private Vector3 offsetPos = new Vector3(-0.43f, 1.6f, -0.3f);
  [SerializeField]
  private Space offsetSpace = Space.Self;
  [SerializeField]
  private bool lookAt = false;

  void LateUpdate()
  {
    Refresh();
  }

  void Refresh()
  {
    // relative position
    if (offsetSpace == Space.Self)
      transform.position = target.TransformPoint(offsetPos);
    else
      transform.position = target.position + offsetPos;

    // relative rotation
    if (lookAt)
      transform.LookAt(target);
    else
      transform.rotation = target.rotation;
  }
}
